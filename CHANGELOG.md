# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.11](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.10...v1.0.11) (2019-08-26)

### [1.0.10](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.9...v1.0.10) (2019-08-26)

### [1.0.9](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.8...v1.0.9) (2019-06-18)


### Bug Fixes

* **max-len:** provides value for `ignoreComments` ([aa877e1](https://gitlab.com/spartanbio-ux/eslint-config/commit/aa877e1))



### [1.0.8](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.7...v1.0.8) (2019-06-18)


### Bug Fixes

* **max-len:** ignores comments ([f4a21d5](https://gitlab.com/spartanbio-ux/eslint-config/commit/f4a21d5))



### [1.0.7](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.6...v1.0.7) (2019-06-17)


### Bug Fixes

* **max-len:** ignores strings and URLS ([7a90e07](https://gitlab.com/spartanbio-ux/eslint-config/commit/7a90e07))



### [1.0.6](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.5...v1.0.6) (2019-06-17)


### Bug Fixes

* switches to `consistent` for array newlines ([b3e6b87](https://gitlab.com/spartanbio-ux/eslint-config/commit/b3e6b87))



### [1.0.5](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.4...v1.0.5) (2019-06-14)


### Bug Fixes

* don't sort imports ([49a2227](https://gitlab.com/spartanbio-ux/eslint-config/commit/49a2227))



### [1.0.4](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.3...v1.0.4) (2019-06-14)


### Bug Fixes

* configures camelcase correctly ([248ff3c](https://gitlab.com/spartanbio-ux/eslint-config/commit/248ff3c))



### [1.0.3](https://gitlab.com/spartanbio-ux/eslint-config/compare/v1.0.2...v1.0.3) (2019-06-14)


### Bug Fixes

* includes eslint-plugin-jest in peerDependencies ([ee624a0](https://gitlab.com/spartanbio-ux/eslint-config/commit/ee624a0))



### 1.0.1 (2019-06-14)



## 1.0.0 (2019-06-14)
- Initial release